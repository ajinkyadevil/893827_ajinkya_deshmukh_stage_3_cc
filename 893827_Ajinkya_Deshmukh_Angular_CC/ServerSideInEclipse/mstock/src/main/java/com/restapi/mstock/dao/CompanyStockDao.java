package com.restapi.mstock.dao;

import java.util.List;

import com.restapi.mstock.models.CompanyDetails;

public interface CompanyStockDao {
	public CompanyDetails find(int companycode);
	public List<CompanyDetails> findAll();
	public boolean save(CompanyDetails details);
	public boolean update( int companycode, CompanyDetails details);
	public boolean delete(int companycode);
	

}
