package com.restapi.mstock.dao;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import com.restapi.mstock.models.CompanyDetails;

@Component
public class CompanyStockDaoImpl implements CompanyStockDao {

	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Override
	public CompanyDetails find(int companycode) {

		return null;
	}

	@Override
	public List<CompanyDetails> findAll() {
		return jdbcTemplate.query("select * from companydetails", new CompanyStockRowMapper());
	}

	@Override
	public boolean save(CompanyDetails details) {

		return false;
	}

	@Override
	public boolean update(int companycode, CompanyDetails details) {

		return false;
	}

	@Override
	public boolean delete(int companycode) {

		return false;
	}

}
