package com.restapi.mstock.dao;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import com.restapi.mstock.models.Stock;

@Component
public class StockDaoImpl implements StockDao {

	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Override
	public List<Stock> getstockcompare(String company,String fromdate, String todate) {
		// TODO Auto-generated method stub
		System.out.println(fromdate+" "+todate);
	
		
	
		
		String query="select id, companyname,stockdate,stockprice from stockcompare where companyname='"+company+"'and stockdate between '"+fromdate+"' and '"+todate+"'";
			return jdbcTemplate.query(query,new StockRowMapper());
	
	}

}
