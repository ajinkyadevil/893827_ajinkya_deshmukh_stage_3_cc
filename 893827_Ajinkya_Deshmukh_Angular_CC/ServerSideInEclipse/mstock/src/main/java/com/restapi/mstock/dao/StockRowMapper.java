package com.restapi.mstock.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.restapi.mstock.models.Stock;

public class StockRowMapper implements RowMapper<Stock>{

	@Override
	public Stock mapRow(ResultSet rs, int rowNum) throws SQLException {
		Stock details = new Stock();
		details.setId(rs.getInt(1));
		details.setCompanyname(rs.getString(2));
		details.setDate(rs.getDate(3));
		details.setStockprice(rs.getDouble(4));
		
		return details;
	}

	
}
