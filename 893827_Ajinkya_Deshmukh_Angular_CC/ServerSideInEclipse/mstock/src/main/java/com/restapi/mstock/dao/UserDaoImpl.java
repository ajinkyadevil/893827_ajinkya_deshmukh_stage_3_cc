package com.restapi.mstock.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import com.restapi.mstock.models.UserDetails;

@Component
public class UserDaoImpl implements UserDao {

	@Autowired
	private JdbcTemplate jdbcTemplate;
	@Override
	public UserDetails find(String email) {

		
		return jdbcTemplate.query("select * from user_tbl where email=?", new UserResultSetExtractor(),email);
		
	}

}
