package com.restapi.mstock.models;

import java.util.Date;

public class Stock {

	int id;
	String companyname;
	
	Date date;
	double stockprice;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getCompanyname() {
		return companyname;
	}
	public void setCompanyname(String companyname) {
		this.companyname = companyname;
	}
	
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public double getStockprice() {
		return stockprice;
	}
	public void setStockprice(double stockprice) {
		this.stockprice = stockprice;
	}
	public Stock(int id, String companyname, Date date, double stockprice) {
		super();
		this.id = id;
		this.companyname = companyname;
		this.date = date;
		this.stockprice = stockprice;
	}
	public Stock() {
		super();
	}
	@Override
	public String toString() {
		return "Stock [id=" + id + ", companyname=" + companyname + ", date=" + date + ", stockprice=" + stockprice
				+ "]";
	}

	
	
}
